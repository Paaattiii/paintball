package de.dieunkreativen.paintball;

import java.util.logging.Logger;

import mc.alk.arena.BattleArena;

import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;

public class Paintball extends JavaPlugin {
	Logger log;
	static Paintball plugin;
	PluginDescriptionFile pdfFile = this.getDescription();
	
    @Override
    public void onLoad() {
            log = getLogger();
    }
	
	@Override
	public void onEnable() {
		plugin = this;
		log.info(pdfFile.getName() + " version " + pdfFile.getVersion() + " is enabled!");
		BattleArena.registerCompetition(this, "Paintball", "pb", PaintballArena.class);

	}
	@Override
	public void onDisable() {
		log.info(pdfFile.getName() + " version " + pdfFile.getVersion() + " is disabled!");
	}
	
	public static Paintball getSelf() {
		return plugin;
	}
	
}
	

	

	



