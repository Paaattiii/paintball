package de.dieunkreativen.paintball;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

import mc.alk.arena.objects.ArenaPlayer;
import mc.alk.arena.objects.arenas.Arena;
import mc.alk.arena.objects.events.ArenaEventHandler;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Effect;
import org.bukkit.FireworkEffect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.Snowball;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

public class PaintballArena extends Arena {
	int task1;
	private HashMap<String, Long> lastShot = new HashMap<String, Long>();
	  
	public void onStart() {
		Set<ArenaPlayer> players = match.getPlayers();
		for (ArenaPlayer arenaPlayer : players) {
			lastShot.put(arenaPlayer.getName(), Long.valueOf(new Date().getTime()));
		}
	}
	
	public void onFinish() {
		Bukkit.getScheduler().cancelTask(task1);
	}
	  
	@ArenaEventHandler(suppressCastWarnings=true)
	public void onEntityDamage(EntityDamageEvent event) {
		if(event.getEntity() instanceof Player) {
	        if(event.getCause().equals(DamageCause.FALL)){
	        	event.setCancelled(true);
	        }
	    }
		if (event.isCancelled() || !(event instanceof EntityDamageByEntityEvent) ||
				((EntityDamageByEntityEvent)event).getDamager().getType() != EntityType.SNOWBALL) {
			return;
		}
        event.setDamage(30.0);
	}
	
	@ArenaEventHandler
	public void onPlayerDeath(final PlayerDeathEvent e)
	{
		e.setDeathMessage(null);
		Player player = e.getEntity();
	    if ((player.getKiller() instanceof Player)) {
	        final Player p = (Player)player;
	        Player killer = p.getKiller();
	        broadcastDeath(killer,p);
	    
		
	        new BukkitRunnable()
	        {
	        	public void run()
	        	{
	        		try
	        		{
	        			Object nmsPlayer = e.getEntity().getClass().getMethod("getHandle").invoke(e.getEntity());
						Object con = nmsPlayer.getClass().getDeclaredField("playerConnection").get(nmsPlayer);
					
						Class< ? > EntityPlayer = Class.forName(nmsPlayer.getClass().getPackage().getName() + ".EntityPlayer");
					
						Field minecraftServer = con.getClass().getDeclaredField("minecraftServer");
						minecraftServer.setAccessible(true);
						Object mcserver = minecraftServer.get(con);
					
						Object playerlist = mcserver.getClass().getDeclaredMethod("getPlayerList").invoke(mcserver);
						Method moveToWorld = playerlist.getClass().getMethod("moveToWorld" , EntityPlayer , int.class , boolean.class);
						moveToWorld.invoke(playerlist , nmsPlayer , 0 , false);
					
						Set<ArenaPlayer> players = match.getPlayers();
						for (ArenaPlayer arenaPlayer : players) {
							arenaPlayer.getPlayer().showPlayer(p);
						}
	        		}
	        		catch (Exception ex)
	        		{
	        			ex.printStackTrace();
	        		}
	        	}
	        }.runTaskLater(Paintball.getSelf() , 4);
	    }
	}
	
	@ArenaEventHandler
	public void onInteract(PlayerInteractEvent event) {
		if(event.getPlayer().getItemInHand().getType() == Material.IRON_SPADE && (event.getAction() == Action.RIGHT_CLICK_AIR || event.getAction() == Action.RIGHT_CLICK_BLOCK)) {
			Player p = event.getPlayer();
			feedArenaPlayer();
			long date = new Date().getTime();
			if (date - ((Long)this.lastShot.get(p.getName())).longValue() > 1000) {
				shootSnowball(p);
				lastShot.put(p.getName(), Long.valueOf(date));
			}
		}
	}
	
	public void shootSnowball(Player p) {
		Vector v = p.getLocation().getDirection().normalize().multiply(3);
		final Projectile snowball = p.launchProjectile(Snowball.class);
		snowball.setMetadata("SnowballToFire", new FixedMetadataValue(Paintball.getSelf(), true));
		snowball.setVelocity(v);
		p.playSound(p.getLocation(), Sound.BLOCK_METAL_PRESSUREPLATE_CLICK_ON , 1F, 0);
		
		
        new BukkitRunnable()
        	{
        	private Location oldLocation;
        	private int timer = 100;
          
        	 public final void run(){
        		 if ((timer-- <= 0) || (!snowball.isValid())) {
        			 cancel();
        		 }
        		 else if (oldLocation == null) {
        			 oldLocation = snowball.getLocation();
        		 }
        		 else {
        			 Location newLoc = snowball.getLocation();
        			 if (oldLocation.toVector().equals(newLoc.toVector())) {
        				 cancel();
        			 }
        			 oldLocation = newLoc;
        		 }
        		 if (oldLocation != null) {
        			 try {
        				ParticleLibrary effect = new ParticleLibrary(ParticleLibrary.ParticleType.REDSTONE, 2, 20, 0.2);
     				    effect.sendToLocation(snowball.getLocation());
        			 }
        			 catch (IllegalArgumentException e) {
        				 e.printStackTrace();
        			 }
        			 catch (Exception e) {
        				 e.printStackTrace();
        			 }
        		 }
        	 }
        	}.runTaskTimer(Paintball.getSelf(), 1L, 2L);
        }
	
	public void feedArenaPlayer() {
		Set<ArenaPlayer> players = match.getPlayers();
		for (ArenaPlayer arenaPlayer : players) {
			arenaPlayer.setFoodLevel(20);
		}
	}
	
	public void broadcastDeath(Player shooter, final Player damaged) {
		shooter.playSound(shooter.getLocation(), Sound.ENTITY_ARROW_HIT_PLAYER , 1F, 0);
		CustomEntityFirework.spawn(damaged.getLocation(), getRandomEffect());
		Set<ArenaPlayer> players = match.getPlayers();
		for (ArenaPlayer arenaPlayer : players) {
			arenaPlayer.sendMessage(ChatColor.GREEN + damaged.getName() + ChatColor.WHITE + " wurde von " + ChatColor.RED + shooter.getName() + ChatColor.WHITE + " abgeschossen!");
		}
	}
	
	public static FireworkEffect getRandomEffect() {
		Color color [] = {
				Color.WHITE,
				Color.RED,
				Color.AQUA,
				Color.GREEN,
				Color.BLUE,
				Color.YELLOW
		};
		
		int size = color.length;
		int randomint = (int) Math.abs(Math.random()*size);
		return FireworkEffect.builder().with(Type.BALL).withColor(color[randomint]).build();
		}
}
	


